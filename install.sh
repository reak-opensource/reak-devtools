#!/bin/bash

# Set System Wallpaper
cd $HOME/Pictures/
curl -s "https://gitlab.com/reak-opensource/reak-devtools/raw/master/wallpaper.png" -o wallpaper.png
xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitor0/image-path --set $HOME/Pictures/wallpaper.png

# Get the Script
sudo curl -s "https://gitlab.com/reak-opensource/reak-devtools/raw/master/devtool.sh" -o /usr/local/bin/devtool
sudo chmod +x /usr/local/bin/devtool
# Set ascii on .bashrc
cat << EndOfLogo > ~/.bashrc
echo -e ".########..########....###....##....##"
echo -e ".##.....##.##.........##.##...##...##."
echo -e ".##.....##.##........##...##..##..##.."
echo -e ".########..######...##.....##.#####..."
echo -e ".##...##...##.......#########.##..##.."
echo -e ".##....##..##.......##.....##.##...##."
echo -e ".##.....##.########.##.....##.##....##"
EndOfLogo