#!/bin/bash


## Functions ##

function showhelp {
## Help Text ##
cat << EndOfHelp

$(tput setaf 1)Welcome to REAK DevTools !

$(tput setaf 3)Help :
$(tput setaf 5)
||| For Phalcon Applications |||
$(tput sgr0)
devtool phalcon create {PROJECT_NAME}
devtool phalcon controller {CONTROLLER_NAME}

Earlier syntax of phalcon {command} {param} has been changed to devtool run phalcon {command} {param}
$(tput setaf 5)
||| For Python Applications |||
$(tput setaf 6)
[For Python2]
$(tput sgr0)

devtool python core
- This installs default python2 virtual environment
$(tput setaf 6)
[For Python3]
$(tput sgr0)

devtool python3 core
- This installs default python3 virtual environment

	
EndOfHelp
}

function showlogo {
cat << EndOfLogo
$(tput setaf 4)
.########..########....###....##....##
.##.....##.##.........##.##...##...##.
.##.....##.##........##...##..##..##..
.########..######...##.....##.#####...
.##...##...##.......#########.##..##..
.##....##..##.......##.....##.##...##.
.##.....##.########.##.....##.##....##
$(tput sgr0)
EndOfLogo

}

function checkupdate {
thisversion="101"
versionfilelink="https://gitlab.com/reak-opensource/reak-devtools/raw/master/.version"
remoteversion=$(curl -s "${versionfilelink}")
if [ $remoteversion -gt $thisversion ];
then
	cd /usr/local/bin
	sudo rm -rf devtool
	curl -s "https://gitlab.com/reak-opensource/reak-devtools/raw/master/devtool.sh" -o "devtool"
	sudo chmod +x devtool
	clear
	echo "||| DevTool has been updated !! |||"
	echo "Please run devtool, to see the new additions"
else
	echo "||| DevTool doesn't have any new version ! |||"
fi
}

## END OF FUNCTIONS ##

if [ -z "$1" ]
then
	clear
	showlogo
	showhelp
fi

if [ "$1" == "help" ]
then
	clear
	showlogo
	showhelp
fi

if [ "$1" == "update" ]
then
	checkupdate
fi

if [ "$1" == "phalcon" ]
then
	# Check for .git file, Confirm with user if they want to proceed
	echo "Have you already created a git repository to be used for this project? (Y/N)"
	read confirmgit
	if [ "$confirmgit" == "Y" ] || [ "$confirmgit" == "y" ]
	then
		if [ "$2" == "project" ]
		then
			# Create gitignore file
			echo -e "nbproject/\n.DS_Store\n$phalconproject/app/config/config.php\n$phalconproject/cache/*" > .gitignore
		fi
		# Need to execute Phalcon with the params
		phalcon $2 $3 $4 $5 $6 $7 $8 $9
	else
		exit 1
	fi
fi

if [ "$1" == "python" ] && [ "$2" == "core" ]
then
	echo -e "Have you already created a git repository to be used for this project? (Y/N)"
	read confirmgit
	if [ "$confirmgit" == "Y" ] || [ "$confirmgit" == "y" ]
	then
		echo -e "nbproject/\n.DS_Store\nvenv/" > .gitignore
		virtualenv -p python venv
		source venv/bin/activate
	fi
fi

if [ "$1" == "python3" ] && [ "$2" == "core" ]
then
	echo -e "Have you already created a git repository to be used for this project? (Y/N)"
	read confirmgit
	if [ "$confirmgit" == "Y" ] || [ "$confirmgit" == "y" ]
	then
		echo -e "nbproject/\n.DS_Store\nvenv/" > .gitignore
		virtualenv -p python3 venv
		source venv/bin/activate
	fi
fi